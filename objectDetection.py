import cv2
import argparse
import numpy as np
import os, fnmatch, shutil
from shutil import copyfile


ap = argparse.ArgumentParser()
ap.add_argument('-c', '--config', required=True,
                help = 'path to yolo config file')
ap.add_argument('-w', '--weights', required=True,
                help = 'path to yolo pre-trained weights')
args = ap.parse_args()


def get_output_layers(net):
    
    layer_names = net.getLayerNames()
    
    output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]

    return output_layers

def getClassIds(imgName, net):

	image = cv2.imread(imgName)
	scale = 0.00392



	blob = cv2.dnn.blobFromImage(image, scale, (416,416), (0,0,0), True, crop=False)

	net.setInput(blob)

	outs = net.forward(get_output_layers(net))

	class_ids = []
	confidences = []
	conf_threshold = 0.3


	for out in outs:
	    for detection in out:
	        scores = detection[5:]
	        class_id = np.argmax(scores)
	        confidence = scores[class_id]
	        if confidence > conf_threshold:
	            class_ids.append(class_id)
	            confidences.append(float(confidence))

	print(class_ids)
	print(confidences)
	return class_ids

folder = 'folder3'
for the_file in os.listdir(folder):
    file_path = os.path.join(folder, the_file)
    try:
        if os.path.isfile(file_path):
            os.unlink(file_path)
        #elif os.path.isdir(file_path): shutil.rmtree(file_path)
    except Exception as e:
        print(e)


net = cv2.dnn.readNet(args.weights, args.config)

listOfFiles = os.listdir('./folder2/')  
pattern = "*.jpeg"
searchImg = '';  
for entry in listOfFiles:  
    if fnmatch.fnmatch(entry, pattern):
            searchImg = './folder2/' + entry
searchIds = getClassIds(searchImg, net) 
print (searchImg)          

listOfFiles = os.listdir('./folder1/')  
for entry in listOfFiles:  
    if fnmatch.fnmatch(entry, pattern):
            searchImg = './folder1/' + entry
            searchIds1 = getClassIds(searchImg, net) 
            if searchIds1 == searchIds:
                print (searchImg)
                copyfile(searchImg, './folder3/' + entry)            